import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({providedIn: 'root'})
export class ServiceNameService {
  constructor(private httpClient: HttpClient) { }
}

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  host: string;
  catData: any;
  listData: any;

  constructor(public http: HttpClient) { 
    this.host = 'https://api.publicapis.org/';
  }

  getCategories(){
    var temp = this.http.get(this.host + "categories");
     this.catData = temp;
    return this.catData;
  }

  getCategoryList(val){
    var temp = this.http.get(this.host + "entries?category="+val+"&https=true");
    this.listData = temp;
    return this.listData;
  }

  filterItems(data, term){
    //console.log(this.catData);
    return data.filter((item) => {
      return item.toLowerCase().includes(term.toLowerCase());
    });
  }

}
