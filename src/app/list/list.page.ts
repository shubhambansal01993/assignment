import { Component, OnInit } from '@angular/core';
import {ApiService} from '../providers/api.service';

@Component({
  selector: 'app-list',
  templateUrl: 'list.page.html',
  styleUrls: ['list.page.scss']
})
export class ListPage implements OnInit {
  private selectedItem: any;
  
  public items: Array<{ title: string; note: string; icon: string }> = [];
  public categories: any = [];
  public data: [];
  list: [];
  showCategory: boolean= true;
  showList: boolean = false;
  searchTerm : any="";
  val: any="";
  constructor(public apiService: ApiService) {
    
  }

  ngOnInit() {
    this.getCategories();  
  }

  // ionViewDidLoad(){
  //   this.setFilteredItems();
  // }
  

  setFilteredItems() {
    this.categories = this.apiService.filterItems(this.categories, this.searchTerm);
  }

  getCategories(){
    this.apiService.getCategories().subscribe((res: any)=> {
      console.log(res);
      this.categories = res;
      console.log(this.categories);
    },error => {
      console.log(error);
      alert("Some error please try after some time.");
    })
  }

  getCategoryList(val){
    console.log(val);
    this.apiService.getCategoryList(val).subscribe((res: any)=>{
      console.log(res);
      this.list = res.entries;
      this.showCategory = false;
      this.showList = true;
    }, error => {
      console.log(error);
      alert("Some error please try after some time.");
    })
  }

  goToCategory(){
    this.showCategory = true;
    this.showList = false;
  }

  getItems(event){
    console.log(event);
  }

  onCancel(){
    console.log('fghjkl')
    this.getCategories();
  }

  

}
